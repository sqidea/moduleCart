package net.flowas.modulecart.rest;

import java.util.HashMap;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

/**
 * 
 */
public class ConfigerationEndpoint
{
   @GET
   @Path("ui.json")
   @Produces("application/json")
   public HashMap listAll(@QueryParam("start") Integer startPosition, @QueryParam("max") Integer maxResult)
   {
	 HashMap map=new HashMap();
	 HashMap  plugins=new HashMap();
	 HashMap  secure=new HashMap();
	 secure.put("url", "login.jsp");
	 secure.put("pageFragment", "loginInfo.jsp");
	 secure.put("label", "登录");
	 plugins.put("secure", secure)    ;
	 map.put("plugins", plugins);
	 return map;
   }
}