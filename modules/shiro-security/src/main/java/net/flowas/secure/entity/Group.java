package net.flowas.secure.entity;

import java.util.Collection;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Organization
 *
 */
@Entity
@Table(name = "ss_group")
public class Group extends IdEntity {

    private static final long serialVersionUID = 1L;

    public Group() {
        super();
    }    
    private String name;
    private String code;
    private String description;    
    private Integer sortOrder;
    @ManyToOne
    private Group parent;

    @OneToMany
    private Collection<Group> children;

   public Group getParent() {
        return parent;
    }

    public void setParent(Group parent) {
        this.parent = parent;
    }

    public Collection<Group> getChildren() {
        return children;
    }

    public void setChildren(Collection<Group> children) {
        this.children = children;
    }
    

//    public BaseEntity getDetial() {
//        return detial;
//    }
//
//    public void setDetial(BaseEntity detial) {
//        this.detial = detial;
//    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the sortOrder
     */
    public Integer getSortOrder() {
        return sortOrder;
    }

    /**
     * @param sortOrder the sortOrder to set
     */
    public void setSortOrder(Integer sortOrder) {
        this.sortOrder = sortOrder;
    }

}
